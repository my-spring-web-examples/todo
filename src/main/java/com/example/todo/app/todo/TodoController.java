package com.example.todo.app.todo;

import java.util.Collection;
import javax.validation.groups.Default;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.todo.app.todo.TodoForm.TodoCreate;
import com.example.todo.app.todo.TodoForm.TodoDelete;
import com.example.todo.app.todo.TodoForm.TodoFinish;
import com.example.todo.common.exception.BusinessException;
import com.example.todo.domain.model.Todo;
import com.example.todo.domain.service.todo.TodoService;

@Controller
@RequestMapping("todo")
@RequiredArgsConstructor
public class TodoController {

    final TodoService todoService;

    @ModelAttribute
    public TodoForm setUpForm() {
        TodoForm form = new TodoForm();
        return form;
    }

    @GetMapping("list")
    public String list(Model model) {
        Collection<Todo> todos = todoService.findAll();
        model.addAttribute("todos", todos);
        return "todo/list";
    }

    @PostMapping("create")
    public String create(@Validated({ Default.class, TodoCreate.class }) TodoForm todoForm, BindingResult bindingResult,
            Model model, RedirectAttributes attributes) {

        if (bindingResult.hasErrors()) {
            return list(model);
        }

        Todo todo = new Todo();
        todo.setTodoTitle(todoForm.getTodoTitle());

        try {
            todoService.create(todo);
        } catch (BusinessException e) {
            model.addAttribute(e.getLocalizedMessage());
            return list(model);
        }

        attributes.addFlashAttribute("success", "Created successfully!");
        return "redirect:/todo/list";
    }

    @PostMapping("finish")
    public String finish(
            @Validated({ Default.class, TodoFinish.class }) TodoForm form,
            BindingResult bindingResult, Model model,
            RedirectAttributes attributes) {
        if (bindingResult.hasErrors()) {
            return list(model);
        }

        try {
            todoService.finish(form.getTodoId());
        } catch (BusinessException e) {
            model.addAttribute(e.getLocalizedMessage());
            return list(model);
        }

        attributes.addFlashAttribute("success", "Finished successfully!");
        return "redirect:/todo/list";
    }

    @PostMapping("delete")
    public String delete(
            @Validated({ Default.class, TodoDelete.class }) TodoForm form,
            BindingResult bindingResult, Model model,
            RedirectAttributes attributes) {

        if (bindingResult.hasErrors()) {
            return list(model);
        }

        try {
            todoService.delete(form.getTodoId());
        } catch (BusinessException e) {
            model.addAttribute(e.getLocalizedMessage());
            return list(model);
        }

        attributes.addFlashAttribute("success", "Deleted successfully!");
        return "redirect:/todo/list";
    }
}
