package com.example.todo.config;

import javax.sql.DataSource;

import org.apache.ibatis.io.VFS;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.boot.autoconfigure.SpringBootVFS;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyBatisConfig {
    @Bean
    public SqlSessionFactory masterSqlSessionFactory(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setVfs(SpringBootVFS.class);
        VFS.addImplClass(SpringBootVFS.class);

        var config = new org.apache.ibatis.session.Configuration();
        config.getTypeAliasRegistry().registerAliases("com.example.todo.domain.model");
        factoryBean.setConfiguration(config);

        return factoryBean.getObject();
    }
}
