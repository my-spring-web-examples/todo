package com.example.todo.common.exception;

import lombok.experimental.StandardException;

@StandardException
public class BusinessException extends RuntimeException {
}
