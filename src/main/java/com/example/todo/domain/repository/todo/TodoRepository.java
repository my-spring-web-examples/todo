package com.example.todo.domain.repository.todo;

import java.util.Collection;
import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;

import com.example.todo.domain.model.Todo;

@Mapper
public interface TodoRepository {
    Optional<Todo> findById(String todoId);

    Collection<Todo> findAll();

    void create(Todo todo);

    boolean update(Todo todo);

    void delete(Todo todo);

    long countByFinished(boolean finished);
}
